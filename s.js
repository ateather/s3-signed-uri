const aws = require('aws-sdk')
const iam = require('./iam.js')
const s3 = new aws.S3({
    accessKeyId: iam.awsAccess,
    secretAccessKey: iam.awsSecret,
    signatureVersion: 'v4'
})
const ex = require('express')
const bp = require('body-parser')
const cors = require('cors');

const BUCKET_HIRES = "mmtestphotos"
const BUCKET_LOWRES = "mmtestgalleries"
const EXPIRE_TIME_S = 60*60
let app = ex()
app.use(cors());
app.use(bp.json())

let gather = (req, key) => {
    return(req.body && req.body[key]) ? req.body[key] 
        : (req.query && req.query[key]) ? req.query[key]
            : null 
}

let makeParams = (bucket, filename, id) => {
    return {
        Bucket: bucket,
        Key: "1-14-2019/23/" + id + "/" + filename,
        Expires: EXPIRE_TIME_S
    }
}

// TODO: move params to internal config
// TODO: translate to c#
app.get('/sign', (req, response) => {
    let imageId = Math.random()
    let message = { success : false }
    let urls = []

    s3.getSignedUrl('putObject', makeParams(BUCKET_HIRES, "HiRes.jpg", imageId), (err, res) => {
        if (err) {
            console.log(err)
            response.sendStatus(403)
            response.send(err)
        } else {
            console.log("Put: ")
            console.log(res)

            urls.push(res)
            urls.push(s3.getSignedUrl('putObject', makeParams(BUCKET_LOWRES, "LowRes.png", imageId)))
            urls.push(s3.getSignedUrl('putObject', makeParams(BUCKET_LOWRES, "Large.jpg", imageId)))
            urls.push(s3.getSignedUrl('putObject', makeParams(BUCKET_LOWRES, "Small.jpg", imageId)))
            urls.push(s3.getSignedUrl('putObject', makeParams(BUCKET_LOWRES, "Thumb.jpg", imageId)))

            message.success = true
            message.urls = urls
        }
        response.json(message)
        response.end()
    })
})

app.listen(3000)